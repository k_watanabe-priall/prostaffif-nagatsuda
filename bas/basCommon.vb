﻿Imports System.IO
Imports System.Text
Imports Oracle.DataAccess.Client
Imports Npgsql
Imports System.Data.SqlClient

Module basCommon

    ''' <summary>
    ''' subOutLog
    ''' </summary>
    ''' <param name="Proc">0・・・Inf、2・・・War、-9・・・Err</param>
    ''' <param name="msg">出力メッセージ</param>
    Public Sub subOutLog(ByVal Proc As Integer, ByVal msg As String, ByVal strMethod As String)
        Dim strOutMsg As String = vbNullString
        Dim strPath As String = My.Settings.LogPath
        Dim strYYYY As String = Now.Year.ToString
        Dim strMM As String = Now.Month.ToString("0#")
        Dim strDD As String = Now.Day.ToString("0#")
        Dim strDateTime As String = Now.ToString("yyyy/MM/dd HH:mm:ss")

        If strPath.EndsWith("\") = False Then
            strPath += "\"
        End If

        '---ログ出力用のフォルダ(yyyy)がなければ作成する。
        If System.IO.Directory.Exists(strPath & strYYYY & "\") = False Then
            FileSystem.MkDir(strPath & strYYYY)
        End If

        '---ログ出力用のフォルダ(mm)がなければ作成する。
        If System.IO.Directory.Exists(strPath & strYYYY & "\" & strMM) = False Then
            FileSystem.MkDir(strPath & strYYYY & "\" & strMM)
        End If

        Select Case Proc
            Case RET_NORMAL
                strOutMsg = "[INF](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
            Case RET_WARNING
                strOutMsg = "[WAR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
            Case RET_ERROR
                strOutMsg = "[ERR](" & strDateTime & ")[" & strMethod & "][" & msg & "]"
        End Select

        '---ログファイルのフルパス生成
        strPath = strPath & "\" & strYYYY & "\" & strMM & "\" & strYYYY & strMM & strDD & ".log"

        '---ログファイルに出力
        Dim writer As StreamWriter = New StreamWriter(strPath, True, Encoding.GetEncoding("shift_jis"))
        writer.WriteLine(strOutMsg)
        writer.Close()
    End Sub

    ''' <summary>
    ''' Oracle接続
    ''' </summary>
    Public Function ConnectionOpenOracle() As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim ConnectionString As String = My.Settings.OraConnection


        Try
            connOra = New OracleConnection(ConnectionString)
            connOra.Open()

            Return RET_NORMAL
        Catch ex As OracleException
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            Return RET_ERROR
        End Try
    End Function

    ''' <summary>
    ''' Oracle切断
    ''' </summary>
    ''' <returns></returns>
    Private Function ConnectionCloseOracle() As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try
            If Not IsNothing(connOra) Then
                If connOra.State = ConnectionState.Open Then
                    connOra.Close()
                End If
                connOra.Dispose()
            End If

            Return RET_NORMAL
        Catch ex As OracleException
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            Return RET_ERROR
        End Try
    End Function

    ''' <summary>
    ''' SQL文発行
    ''' </summary>
    ''' <param name="strSQL">SQL文</param>
    ''' <param name="dt">取得結果データテーブル</param>
    ''' <returns></returns>
    Public Function DBQueryOracle(ByVal strSQL As String, ByRef dt As DataTable) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try
            'OracleDB Open
            If ConnectionOpenOracle() = RET_ERROR Then
                Call subOutLog(RET_ERROR, "OracleDB接続エラー", strMethod)
                Return RET_ERROR
            End If

            '---生成SQL文をログ出力
            Call subOutLog(RET_NORMAL, "SQL：" & strSQL, strMethod)

            Dim cmd As OracleCommand = New OracleCommand(strSQL, connOra)
            Dim oda As OracleDataAdapter = New OracleDataAdapter(cmd)
            'dt = New DataTable
            oda.Fill(dt)

            'OracleDB Close
            If ConnectionCloseOracle() = RET_ERROR Then
                Call subOutLog(RET_ERROR, "OracleDB切断エラー", strMethod)
                Return RET_ERROR
            End If

            Return RET_NORMAL
        Catch ex As Exception
            Call subOutLog(RET_ERROR, "エラー内容：" & ex.Message, strMethod)
            Return RET_ERROR
        End Try
    End Function

    ''' <summary>
    ''' Postgret接続
    ''' </summary>
    ''' <returns></returns>
    Public Function ConnectionOpenPostgret() As Boolean
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try

            Dim connString As New NpgsqlConnectionStringBuilder
            '---接続情報の設定
            With connString
                .Host = My.Settings.DBServer        '接続先サーバ
                .Port = My.Settings.DBPort          'DataBase Port
                .Database = My.Settings.DBName      'DataBase名
                .Username = My.Settings.DBUserID    '接続ユーザ
                .Password = My.Settings.DBPass      '接続パスワード
                .Timeout = 0                             'タイムアウト時間
            End With

            connPostgret = New Npgsql.NpgsqlConnection(connString.ConnectionString)

            '---接続状態のチェック
            If connPostgret.State <> ConnectionState.Closed Then
                connPostgret.Close()
            End If

            '---接続
            connPostgret.Open()

            Return True
        Catch ex As Exception
            '---ログ出力
            Call subOutLog(RET_ERROR, ex.Message, strMethod)

            Return False
        End Try
    End Function

    ''' <summary>
    ''' Postgret切断
    ''' </summary>
    ''' <returns></returns>
    Public Function ConnectionClosePostgret() As Boolean
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try
            If connPostgret.State <> ConnectionState.Closed Then
                connPostgret.Close()
            End If

            Return True
        Catch ex As Exception
            '---ログ出力
            Call subOutLog(RET_ERROR, ex.Message, strMethod)

            Return False
        End Try
    End Function

#Region "SQLサーバ接続"

    ''' <summary>
    ''' SQLサーバ接続
    ''' </summary>
    ''' <returns></returns>
    Public Function fncDBConnect() As Boolean
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try

            'ﾛｸﾞ用ﾃｰﾌﾞﾙ接続
            Dim connectionString3 As New System.Text.StringBuilder
            connectionString3.Append("Data Source=")
            connectionString3.Append(My.Settings.DBServer2)
            connectionString3.Append(";")
            connectionString3.Append("Initial Catalog=")
            connectionString3.Append(My.Settings.DBName2)
            connectionString3.Append(";")
            connectionString3.Append("UID=")
            connectionString3.Append(My.Settings.DBUserID2)
            connectionString3.Append(";")
            connectionString3.Append("PWD=")
            connectionString3.Append(My.Settings.DBPass2)
            connectionString3.Append(";")
            connectionString3.Append("Integrated Security=false;")
            connectionString3.Append("Connect Timeout = 0;")

            connSQLServer = New SqlConnection
            With connSQLServer
                .ConnectionString = connectionString3.ToString
                .Open()
            End With

            Return True

        Catch ex As Exception

            Call subOutLog(RET_ERROR, "データベース接続 失敗 (エラー内容 = " & ex.Message & ") ", strMethod)

            Return False

        End Try

    End Function
#End Region

#Region "SQLサーバ切断"
    ''' <summary>
    ''' SQLサーバ切断
    ''' </summary>
    ''' <returns></returns>
    Public Function fncDBDisConnect() As Boolean
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try

            If Not connSQLServer Is Nothing Then
                connSQLServer.Close()
                connSQLServer.Dispose()
            End If

            Return True
        Catch ex As Exception
            Call subOutLog(RET_ERROR, "DisConnect 失敗 (エラー内容 = " & ex.Message & ") ", strMethod)

            Return False
        End Try
    End Function
#End Region

#Region "SQL発行"
    ''' <summary>
    ''' SQL発行
    ''' </summary>
    ''' <param name="objCon"></param>
    ''' <param name="strSQL"></param>
    ''' <returns></returns>
    Public Function fncAdptSQL(ByRef objCon As SqlConnection, ByVal strSQL As String) As DataSet
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name
        Dim dsData As New DataSet

        Try
            Dim objAdpt As SqlDataAdapter = New SqlDataAdapter(strSQL, objCon)

            objAdpt.SelectCommand.CommandTimeout = 0
            objAdpt.Fill(dsData)

            Return dsData

        Catch ex As Exception

            Call subOutLog(RET_ERROR, "データベース接続 失敗 (エラー内容 = " & ex.Message & ") ", strMethod)

            Return Nothing
        Finally
            dsData.Dispose()
        End Try

    End Function

    ''' <summary>
    ''' DB更新
    ''' </summary>
    ''' <param name="objCon"></param>
    ''' <param name="strSQL"></param>
    ''' <returns></returns>
    Public Function fncExecuteNonQuery(ByRef objCon As SqlConnection, ByVal strSQL As String) As Integer
        Dim strMethod As String = System.Reflection.MethodBase.GetCurrentMethod.Name

        Try

            Dim objCommand As SqlCommand

            objCommand = objCon.CreateCommand()
            objCommand.CommandText = strSQL

            Dim iResult As Integer = objCommand.ExecuteNonQuery()
            objCommand.Dispose()

            Return iResult

        Catch ex As Exception

            Call subOutLog(RET_ERROR, "データベース登録 失敗 (エラー内容 = " & ex.Message & ") ", strMethod)

            Return RET_ERROR
        End Try

    End Function

#End Region

End Module
